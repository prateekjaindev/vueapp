# # build stage
# FROM node:lts-alpine as build-stage
# WORKDIR /app
# COPY package*.json ./
# RUN npm install
# COPY . .
# RUN npm run build

# production stage
# FROM cgr.dev/chainguard/nginx:latest as production-stage
FROM nginx:latest
COPY index.html /usr/share/nginx/html/green/
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]